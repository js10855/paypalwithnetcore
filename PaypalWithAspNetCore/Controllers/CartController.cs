﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaypalWithAspNetCore.Models;
using PaypalWithAspNetCore.PayPal;

namespace PaypalWithAspNetCore.Controllers
{
    [Route("cart")]
    public class CartController : Controller
    {
        [Route("")]
        [Route("~/")]
        [Route("index")]
        public IActionResult Index()
        {
            List<Product> products = new List<Product>()
            {
                new Product{Id="p01",Name="name 1",Price=2,Quantity=3},
                new Product{Id="p02",Name="name 2",Price=4,Quantity=2},
                new Product{Id="p03",Name="name 3",Price=5,Quantity=3}
            };
            ViewBag.products = products;
            PayPalConfig payPalConfig = PayPalService.getPayPalConfig();
            ViewBag.payPayConfig = payPalConfig;
            return View();
        }
    }
}