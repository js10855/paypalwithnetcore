﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace PaypalWithAspNetCore.PayPal
{
    public class PDTHolder
    {
        public double GrossTotal { get; set; }
        public int InvoiceNumber { get; set; }
        public string   PaymentStatus { get; set; }
        public string PayerFirstName { get; set; }
        public double PaymentFee { get; set; }
        public  string BusinessEmail { get; set; }
        public string PayerEmail { get; set; }
        public string TxToken { get; set; }
        public string PayerLastName { get; set; }
        public string ReciverEmail { get; set; }
        public string ItemName { get; set; }
        public string Currency { get; set; }
        public string TransactionId { get; set; }
        public string SubscriberId { get; set; }
        public string Custom { get; set; }

        public static string authToken, txToken, query, strResponse;
        public static PDTHolder Success(string tx)
        {
            PayPalConfig payPalConfig = PayPalService.getPayPalConfig();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //authToken = WebConfigurationManager.AppSettings["PDTToken"];
            authToken = payPalConfig.AuthToken;
            txToken = tx;
            query = string.Format("cmd=_notify-synch&tx={0}&at={1}", txToken, authToken);
            //string url= WebConfigurationManager.AppSettings["PayPalSubmitUrl"];
            string url = payPalConfig.PostUrl;
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = query.Length;
            StreamWriter stOut = new StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII);
            stOut.Write(query);
            stOut.Close();
            StreamReader stIn = new StreamReader(req.GetResponse().GetResponseStream());
            strResponse = stIn.ReadToEnd();
            stIn.Close();
            if (strResponse.StartsWith("SUCCESS"))
                return PDTHolder.Parse(strResponse);

            return null;
        }

        public static PDTHolder Parse(string PostData)
        {
            String sKey, sValue;
            PDTHolder ph = new PDTHolder();
            try
            {
                string[] stringArray = PostData.Split('\n');
                int i;
                for (i = 1; i < stringArray.Length-1; i++)
                {
                    string[] stringArray1 = stringArray[i].Split('=');
                    sKey = stringArray1[0];
                    sValue = HttpUtility.UrlDecode(stringArray1[1]);
                }
            }
            catch (Exception ex)
            {

            }
            return ph;
        }
    }
}
